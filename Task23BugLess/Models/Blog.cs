﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task23BugLess.Models
{
    public class Blog
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Min 3, Max 255")]
        public string Header { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Min 3, Max 255")]
        public string Content { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        [Display(Name = "Author Name")]
        public string AuthorName { get; set; }
        [Required]
        [Display(Name = "Creation Time")]
        public DateTime CreationTime { get; set; }
    }
}