﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task23BugLess.Models
{
    public class Feedback
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        [Display(Name = "Author Name")]
        public string AuthorName { get; set; }
        [Required]
        [Display(Name = "Creation Time")]
        public DateTime CreationTime { get; set; }
        [StringLength(255, MinimumLength = 3)]
        public string Content { get; set; }
        public int BlogId { get; set; }
    }
}