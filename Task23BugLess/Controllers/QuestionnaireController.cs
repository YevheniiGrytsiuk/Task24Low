﻿   using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23BugLess.Helpers;
using Task23BugLess.Models;

namespace Task23BugLess.Controllers
{
    public class QuestionnaireController : Controller
    {
        [ActionName("New")]
        [HttpGet]
        public ActionResult CreateNewPost()
        {
            return View();
        }
        [ActionName("New")]
        [HttpPost]
        public ActionResult CreateNewPost(Blog blog)
        {
            if (ModelState.IsValid)
            {
                AddBlog(blog);
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [NonAction]
        private void AddBlog(Blog blog)
        {
            var blogs = BlogHelper.GetBlogs();

            blogs.Add(new Blog() 
            { 
                //fekalis
                Id = blogs.Where(b => b.Id == blog.Id).Any() ? blogs.Last().Id: blog.Id, 
                AuthorName = blog.AuthorName, 
                Content = blog.Content, 
                CreationTime = blog.CreationTime, 
                Header = blog.Header 
            });

        }
    }
}