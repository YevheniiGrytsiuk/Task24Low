﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23BugLess.Helpers;
using Task23BugLess.Models;

namespace Task23.Controllers
{
    public class GuestController : Controller
    {
        private static List<Feedback> _feedbacks;
        static GuestController() 
        {
            _feedbacks = FeedbackHelper.GetFeedbacks();
        }
        public ActionResult Index()
        {
            return View(_feedbacks);
        }
        [HttpGet]
        public ActionResult Feedback(int id)
        {
            var blogFeedback = _feedbacks.Where(feedback => feedback.BlogId == id);
            ViewBag.BlogId = id;

            return View(blogFeedback);
        }
        [HttpPost]
        public RedirectToRouteResult Feedback(Feedback feedback) 
        {
            if (ModelState.IsValid) 
            {
                _feedbacks.Add(feedback);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}