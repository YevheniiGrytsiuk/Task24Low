﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Task23BugLess
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Guest",
                url: "Guest/{action}/{blogId}",
                defaults: new 
                { 
                    controller = "Guest", 
                    action = "Index",
                    blogId = UrlParameter.Optional
                }
            );
            routes.MapRoute(
                name: "Questionnaire",
                url: "Questionnaire/{action}/{id}",
                defaults: new { controller = "Questionnaire", action = "New", id = UrlParameter.Optional }
            );
        }
    }
}
